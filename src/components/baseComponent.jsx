import React from 'react'
import PropTypes from 'prop-types'
import Evt from 'js/event/eventsEmitter'

class BaseComponent extends React.PureComponent {
    constructor(props) {
        super(props)

        this.event = Evt

        // 组件的自定义标识，用于同一个页面存在相同的组件
        const flag = props.flag ? '_' + props.flag : ''
        // 组件名称
        // 思考 当前是通过组件名来作为唯一标识符，使用这种方式webpack的mangle选项true，意思是不压缩变量名，不压缩变量名会导致打包出来的代码比较大
        this.componentName = this.constructor.name + flag
        // 是否为主屏
        this.isMaster = false

        // 监听数据同步事件
        this.event.on('sync', evnet => {
            if (
                this.context.page === evnet.page &&
                this.componentName === evnet.component
            ) {
                this.setState(evnet.data)
            }
        })

        // 有页面加入或退出 这个join应该放在页面处理而不是放在组件里处理，master是针对页面的并不是针对组件，如果一个页面有多个组件，这段代码就会执行多次，产生性能问题
        this.event.on('join', event => {
            event.pages.every(item => {
                if (item.pageId === this.context.pageId && item.master) {
                    this.isMaster = true
                    return false
                }
                return true
            })
            this.joinPage()
        })
    }

    render() {
        return null
    }

    componentWillUnmount() {
        this.removeAllEvent()
    }

    // 移除所有事件
    removeAllEvent() {
        Evt.removeAllListeners()
    }

    // 状态同步
    setStateSync(obj, fun = null) {
        this.setState(obj, () => {
            if (fun) {
                fun()
            }
        })
        if (global.ws) {
            global.ws.emit('sync', {
                page: this.context.page,
                component: this.componentName,
                data: obj
            })
        }
    }

    joinPage() {}
}

BaseComponent.contextTypes = {
    page: PropTypes.string,
    pageId: PropTypes.string
}

export default BaseComponent
