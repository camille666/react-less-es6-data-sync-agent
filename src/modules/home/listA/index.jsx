import React from 'react'
import BaseComponent from 'components/baseComponent'

class ListA extends BaseComponent {
    constructor(props) {
        super(props)
        this.state = {
            scrollTop: 0,
            scrollLeft: 0
        }
        this.isUserScroll = true
    }

    render() {
        return (
            <div
                ref={node => (this.box = node)}
                style={{ width: '200px', height: '200px', overflow: 'auto' }}
                onScroll={this.scrollHandler.bind(this)}
            >
                {this.props.children}
            </div>
        )
    }

    componentDidUpdate(prevProps, prevState) {
        if (
            prevState.scrollLeft !== this.state.scrollLeft ||
            prevState.scrollTop !== this.state.scrollTop
        ) {
            this.isUserScroll = false
            this.box.scrollLeft = this.state.scrollLeft
            this.box.scrollTop = this.state.scrollTop
        }
    }

    scrollHandler() {
        if (this.isUserScroll) {
            this.setStateSync({
                scrollTop: this.box.scrollTop,
                scrollLeft: this.box.scrollLeft
            })
        }
        this.isUserScroll = true
    }
}

export default ListA
