/**
 * 不需要实时更新，热更新
 * css样式提取到单独文件
 * 压缩html，资源
 * 轻量sourcemap
 * 文件名hash处理
 * 资源缓存
 */
// path
const paths = require('./paths.js')
const path = require('path')
const SOURCE_PATH = paths.SOURCE_PATH

// configfile
const merge = require('webpack-merge')
const commonWkConfig = require('./webpack.common.config.js')
const webpack = require('webpack')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin') // 压缩css插件
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const prodLoaders = [
    {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
    },
    {
        test: /\.less$/,
        exclude: /node_modules/,
        use: [
            MiniCssExtractPlugin.loader,
            {
                loader: 'css-loader',
                options: {
                    modules: {
                        localIdentName: '[name]_[local]_[hash:base64:5]'
                    }
                }
            },
            'less-loader'
        ]
    }
]

const prodPlugins = [
    new MiniCssExtractPlugin({
        filename: '[name].[contenthash:8].css',
        chunkFilename: '[name].[contenthash:8].css'
    }),

    new HtmlWebpackPlugin({
        title: '首页',
        filename: 'index.html',
        template: path.resolve(SOURCE_PATH, './template/index.html'),
        inject: true,
        hash: true,
        favicon: path.resolve(SOURCE_PATH, './assets/img/favicon.ico'),
        minify: {
            removeComments: true,
            collapseWhitespace: true,
            removeAttributeQuotes: true
        }
    }),
    new webpack.HashedModuleIdsPlugin()
]

// webpack4，新加属性
const OPTIMIZE = {
    splitChunks: {
        cacheGroups: {
            // 缓存组会继承splitChunks的配置，但是test、priorty和reuseExistingChunk只能用于配置缓存组。
            reactLib: {
                chunks: 'initial',
                test: /[\\/]react[\\/]/,
                name: 'reactLib',
                reuseExistingChunk: true,
                enforce: true
            },
            reactDomLib: {
                chunks: 'initial',
                test: /react-dom/,
                name: 'reactDomLib',
                reuseExistingChunk: true,
                enforce: true
            },
            reactRouterDomLib: {
                chunks: 'initial',
                test: /react-router-dom/,
                name: 'reactRouterDomLib',
                reuseExistingChunk: true,
                enforce: true
            }
        }
    },
    minimizer: [
        // 压缩js、压缩css配置
        new UglifyJsPlugin({
            sourceMap: false,
            cache: true,
            parallel: true
        }),
        new OptimizeCssAssetsPlugin()
    ],
    runtimeChunk: 'single'
}

const prodConfig = merge(commonWkConfig, {
    mode: 'production',
    devtool: false,
    module: { rules: prodLoaders },
    plugins: prodPlugins,
    optimization: OPTIMIZE
})

module.exports = prodConfig
