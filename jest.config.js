'use strict'

module.exports = {
    moduleFileExtensions: ['js', 'jsx'],
    roots: ['<rootDir>/src/tests'],
    setupFilesAfterEnv: ['<rootDir>/src/setUpEnzyme.js'],
    collectCoverage: true,
    collectCoverageFrom: [
        '<rootDir>/src/**/*.{js,jsx}',
        '!<rootDir>/src/**/*.test.js',
        '!<rootDir>/src/**/__*__/*',
        '!**/node_modules/**'
    ]
}
